--@client
--@include	lib/file_browser.lua

local browse = require("lib/file_browser.lua")
browse.setOpen(true)
local background = Color(95, 95, 95, 255)
hook.add("render", "render", function()
	render.setBackgroundColor(background)
	browse.draw()
end)
hook.add("inputPressed", "input", function(btn)
	local handled, path, filename = browse.inputRaw(btn)
	if handled and path and filename then
		print("You selected: "..table.concat(path, "/").."/"..filename)
	end
end)