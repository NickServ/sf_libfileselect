# libfileselect
StarfallEx library for adding file select menus to your projects. Easy to use in new or existing projects.
## How to use
Put the *lib/file_select.lua* file in *garrysmod/data/starfall/lib*.
In your script, add the `--@include lib/file_select.lua` preprocessor directive to the top of the file.
You can then do `require("lib/file_select.lua")` which will return a table containing all of the functions you need.
See the *demo.lua* file for an example usage of this library.
## To-do:
- ~~Add a function that handles raw key input~~ **Implemented**
- ~~Add support for a background color~~ **Implemented**
- Check for edge cases
- Clean up the code
## List of functions
### getSelection()
Returns the number index of the currently highlighted file.
### setSelection(*number* index)
Sets the currently highlighted file based on its number index.
If it is less than 1, it will highlight the last file in the directory.
If it is greater than the number of files in the directory, it will be set to 1.
### getDir()
Returns the current path as a table. To get it as a string, do `table.concat(path, "/")` where `path` is the output of `getDir`.
### setDir(*string/table* path)
Sets the current path. Argument can be either a table formatted the same as the output of `getDir`, or a string with each directory seperated by a slash.
To go to the root directory, you can use any of the following: `setDir("")`, `setDir("/")`, or `setDir({})`.
### getOpen()
Returns whether the file selector is open or not as a boolean. False by default.
### setOpen(*boolean* open)
Sets whether `draw` or `inputRaw` will do anything. False by default.
### getOptions()
Returns a table containing the current options. Values:

- *number* x
- *number* y
- *number* w
- *number* h
- *boolean* draw_background
- *number* padding
- *number* header_height
- *number* bullet_width
- *number* body_height
- *color* background_color
- *color* header_color
- *color* dir_color
- *color* file_color
- *color* bullet_color
- *string* header_font
- *string* body_font

### setOptions(*table* options)
Sets the current options, including position, size, font, colors, etc.
Takes a table as an argument; accepts same values in same format as output of `getOptions`.
### draw()
Draws the file selector to the screen. **Must be done in a render hook.**
### back()
Goes up to the parent directory.
### enter()
If a directory is selected, it will enter the directory. If a file is selected, it will return the current path (table) and selected filename (string).
### inputRaw(*number* key)
Intended to be used in an `inputReceived` hook. If it handled an input, it will return true. If it did not, it will return false.
If the handled input caused a file to be selected, it will also return the output of `enter`.