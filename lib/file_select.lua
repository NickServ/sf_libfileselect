--@name	File select library
--@client

local open = false

local x
local x_off
local y
local y_off
local w
local h

local selection = 1
local max_selection
local offset = 0

local draw_background
local padding = 10
local header_height
local bullet_width
local body_height
local max_lines
local scrollbar_height 

local background_color
local header_color
local dir_color
local file_color
local bullet_color
local scrollbar_bg
local scrollbar_fg
local header_font
local body_font

local path
local path_pretty
local list

local module = {}
module.getSelection = function()
	return selection
end
module.setSelection = function(desired)
	selection = desired
	if selection < 1 then
		selection = max_selection
		if max_selection > max_lines then
			offset = max_selection-max_lines
		end
	elseif selection > max_selection then
		selection = 1
		offset = 0
	elseif selection-offset > max_lines then
		offset = selection-max_lines
	elseif selection-offset < 1 then
		offset = selection-1
	end
end
module.getDir = function()
	return path
end
module.setDir = function(target)
	local str_target
	if type(target) == "string" then
		str_target = string.trim(target)
		target = string.explode("/", str_target)
	else
		str_target = table.concat(target, "/")
	end
	if not file.exists(str_target) then return false end
	path = target
	local path_str = str_target
	path_pretty = "Path: /"..path_str
	local files, dirs = file.find(path_str.."/*")
	list = {
		{"dir", ".."}
	}
	local tmp = {}
	local num_dirs = #dirs
	for i=1, num_dirs do
		list[i+1] = {"dir", dirs[i]}
	end
	local num_files = #files
	for i=1, num_files do
		list[num_dirs+i+1] = {"file", files[i]}
	end
	max_selection = num_dirs+num_files+1
	module.setSelection(math.clamp(selection, 1, max_selection))
	return true
end
module.getOpen = function()
	return open
end
module.setOpen = function(bool)
	open = bool and true or false
end
module.getOptions = function()
	return {
		x = x,
		y = y,
		w = w,
		h = h,
		draw_background = draw_background,
		padding = padding,
		header_height = header_height,
		bullet_width = bullet_width,
		body_height = body_height,
		--max_lines = max_lines,
		background_color = background_color,
		header_color = header_color,
		dir_color = dir_color,
		file_color = file_color,
		bullet_color = bullet_color,
		scrollbar_bg = scrollbar_bg,
		scrollbar_fg = scrollbar_fg,
		header_font = header_font,
		body_font = body_font
	}
end
module.setOptions = function(options)
	--if not options then return end
	x = options.x or x
	y = options.y or y
	w = options.w or w
	h = options.h or h
	draw_background = options.draw_background or draw_background
	padding = options.padding or padding
	header_height = options.header_height or header_height
	bullet_width = options.bullet_width or bullet_width
	body_height = options.body_height or body_height
	--max_lines = options.max_lines or max_lines
	background_color = options.background_color or background_color
	header_color = options.header_color or header_color
	dir_color = options.dir_color or dir_color
	file_color = options.file_color or file_color
	bullet_color = options.bullet_color or bullet_color
	scrollbar_bg = options.scrollbar_bg or scrollbar_bg
	scrollbar_fg = options.scrollbar_fg or scrollbar_fg
	header_font = options.header_font or header_font
	body_font = options.body_font or body_font
	
	x_off = x+padding+bullet_width
	y_off = y+padding+header_height-body_height
	max_lines = math.floor((h-padding-padding-header_height)/body_height)
	scrollbar_height = max_lines*body_height-body_height
end
module.setOptions({
	x = padding,
	y = padding,
	w = 512-padding-padding,
	h = 512-padding-padding,
	draw_background = true,
	padding = 10,
	header_height = 20,
	bullet_width = 20,
	body_height = 10,
	max_lines = 47,
	background_color = Color(0, 0, 0, 127),
	header_color = Color(255, 255, 255, 255),
	dir_color = Color(114, 159, 207, 255),
	file_color = Color(255, 255, 255, 255),
	bullet_color = Color(255, 255, 255, 255),
	scrollbar_bg = Color(255, 255, 255, 47),
	scrollbar_fg = Color(255, 255, 255, 255),
	header_font = render.getDefaultFont(),
	body_font = render.getDefaultFont()
})
module.draw = function()
	if not open then return end
	if draw_background then
		render.setColor(background_color)
		render.drawRectFast(x, y, w, h)
	end
	render.setColor(header_color)
	render.setFont(header_font)
	render.drawText(x+padding, y+padding, path_pretty)
	render.setFont(body_font)
	for i=1, max_lines do
		if not list[i+offset] then return end
		if selection == i+offset then
			render.setColor(bullet_color)
			render.drawText(x_off-bullet_width, y_off+(i*body_height), ">")
		end
		render.setColor(list[i+offset][1] == "dir" and dir_color or file_color)
		render.drawText(x_off, y_off+(i*body_height), list[i+offset][2])
	end
	if max_selection > max_lines then
		render.setColor(scrollbar_bg)
		render.drawRectFast(x+w-12-padding, y_off+header_height, 12, scrollbar_height)
		render.setColor(scrollbar_fg)
		local sbh2 = max_lines/max_selection
		local sbyo = offset/max_selection
		-- It took me a long time to get this right
		--render.drawText(256, 256, string.format("selection %u\noffset %u\nsbh2 %s\nsbyo %s\nsum %s %s", selection, offset, sbh2, sbyo, sbyo+sbh2, sbyo+sbh2 > 1 and "(BAD)" or ""))
		render.drawRectFast(x+w-12-padding, y_off+header_height+(scrollbar_height*sbyo), 12, scrollbar_height*sbh2)
	end
end
module.back = function()
	table.remove(path)
	if not module.setDir(path) then
		path = {}
		module.setDir(path)
	end
end
module.enter = function()
	local newpath = table.copy(path)
	if list[selection][1] == "file" then
		return path, list[selection][2]
	elseif selection == 1 then
		table.remove(newpath)
	else
		table.insert(newpath, list[selection][2])
	end
	module.setDir(newpath)
end
module.inputRaw = function(btn)
	if not open then
		return false
	end
	if btn == 88 then
		module.setSelection(selection-(input.isShiftDown() and 10 or 1))
	elseif btn == 90 then
		module.setSelection(selection+(input.isShiftDown() and 10 or 1))
	elseif btn == 91 or btn == 64 then
		return true, module.enter()
	elseif btn == 89 or btn == 66 then
		module.back()
	else
		return false
	end
	return true
end

module.setDir({})
return module